import React from "react";
import { Card, CardContent, Grid, Typography } from "@material-ui/core";
import cx from "classnames";
import CountUp from "react-countup";

import styles from "./DataCard.module.css";

interface DataCardProps {
    title: string;
    cases: number;
    lastUpdate: string;
    description: string;
    className: string;
}

const DataCard: React.FC<DataCardProps> = (props: DataCardProps) => {
    const { title, cases, lastUpdate, description, className } = props;
    return (
        <Grid item xs={12} md={3} component={Card} className={cx(className, styles.card)}>
            <CardContent>
                <Typography color='textSecondary' gutterBottom>
                    {title}
                </Typography>
                <Typography variant='h5'>
                    <CountUp start={0} end={cases} separator='.' duration={3} />
                </Typography>
                <Typography color='textSecondary'>{new Date(lastUpdate).toDateString()}</Typography>
                <Typography variant='body2'>{description}</Typography>
            </CardContent>
        </Grid>
    );
};

export default DataCard;
