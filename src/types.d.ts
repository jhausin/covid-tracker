interface CovidData {
    confirmed: { value: number };
    recovered: { value: number };
    deaths: { value: number };
    lastUpdate: string;
}
