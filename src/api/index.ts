import axios from "axios";

const url: string = "https://covid19.mathdro.id/api";

export const fetchData = async (country?: string): Promise<CovidData | undefined> => {
    let countryUrl: string = url;

    if (country) {
        countryUrl = `${url}/countries/${country}`;
    }
    try {
        const {
            data: { confirmed, recovered, deaths, lastUpdate },
        } = await axios.get(countryUrl);

        return { confirmed, recovered, deaths, lastUpdate };
    } catch (error) {
        console.log(error.message);
    }
};

export const fetchCountries = async () => {
    try {
        const {
            data: { countries },
        } = await axios.get(`${url}/countries`);
        return countries.map((country: { name: string }) => country.name);
    } catch (error) {}
};
