import React, { useEffect, useState } from "react";
import { FormControl, NativeSelect } from "@material-ui/core";

import { fetchCountries } from "../../api";
import styles from "./CountryPicker.module.css";

interface CountryPickerProps {
    handleCountryChange(event: string): Promise<void>;
}

const CountryPicker: React.FunctionComponent<CountryPickerProps> = ({ handleCountryChange }) => {
    const [countries, setCountries] = useState([]);
    useEffect(() => {
        const fetchApi = async () => {
            setCountries(await fetchCountries());
        };
        fetchApi();
    }, []);
    return (
        <FormControl className={styles.form}>
            <NativeSelect defaultValue='' onChange={(e) => handleCountryChange(e.target.value)}>
                <option value=''>Global</option>
                {countries.map((country, i) => (
                    <option key={i} value={country}>
                        {country}
                    </option>
                ))}
            </NativeSelect>
        </FormControl>
    );
};

export default CountryPicker;
