import React from "react";
import { Grid, Typography } from "@material-ui/core";

import DataCard from "./DataCard/DataCard";
import styles from "./Cards.module.css";

interface CardProps {
    data: CovidData;
}

const Cards: React.FC<CardProps> = (props: CardProps) => {
    const { confirmed, recovered, deaths, lastUpdate } = props.data;
    if (!confirmed) {
        return <h1>"Loading ..."</h1>;
    }
    return (
        <div className={styles.container}>
            <Typography variant='h4' component='h2' gutterBottom>
                Global
            </Typography>
            <Grid container spacing={3} justify='center'>
                <DataCard
                    title='Infected'
                    cases={confirmed.value}
                    lastUpdate={lastUpdate}
                    description='Number of active cases from COVID-19.'
                    className={styles.infected}
                />
                <DataCard
                    title='Recovered'
                    cases={recovered.value}
                    lastUpdate={lastUpdate}
                    description='Number of recovered cases from COVID-19'
                    className={styles.recovered}
                />
                <DataCard
                    title='Deaths'
                    cases={deaths.value}
                    lastUpdate={lastUpdate}
                    description='Number of deaths from COVID-19'
                    className={styles.deaths}
                />
            </Grid>
        </div>
    );
};

export default Cards;
