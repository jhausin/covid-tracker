import React, { useState } from "react";
interface ChartProps {
    data: {
        confirmed: number;
        recovered: number;
        deaths: number;
        lastUpdate: string;
    };
    country: string;
}

const Chart: React.FunctionComponent<ChartProps> = ({
    data: { confirmed, recovered, deaths, lastUpdate },
    country,
}) => {
    const [countryData, setCountryData] = useState({});
    // useEffect(() => {
    //     const fetchApi = async () => {
    //         // const fetchedData = await fetchCountryData();x
    //     }
    // }, []);
    return <div>Chart</div>;
};

export default Chart;
