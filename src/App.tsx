import React from "react";
import { Cards, Chart, CountryPicker } from "./components";
import styles from "./App.module.css";
import { fetchData } from "./api";

interface AppState {
    data?: CovidData;
    country?: string;
}
class App extends React.Component<{}, AppState> {
    state: AppState = {
        data: undefined,
        country: "",
    };
    async componentDidMount() {
        const fetchedData: CovidData | undefined = await fetchData();
        console.log(fetchedData);
        this.setState({ data: fetchedData });
    }
    handleCountryChange = async (country: string) => {
        const data = await fetchData();
        this.setState({ data, country: country });
    };
    render() {
        const { data, country } = this.state;
        if (data) {
            return (
                <div className={styles.container}>
                    <Cards data={data} />
                    <CountryPicker handleCountryChange={this.handleCountryChange} />
                    {/* <Chart country={country}/> */}
                </div>
            );
        }
        return <h1>Loading ...</h1>;
    }
}

export default App;
